package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    public Ponto(){
        
    }
    public Ponto(double x,double y ,double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    @Override
    public String toString(){
        return String.format("%s(%f,%f,%f)",getNome(),this.x,this.y,this.z);
    }
    @Override
    public boolean equals(Object x){
        Ponto comp = (Ponto)x;
        int indicadora = 1;
        if(comp.x != this.x){
            indicadora = 0;
        }
        else if(comp.y != this.y){
            indicadora = 0;
        }
        else if(comp.z != this.z){
            indicadora = 0;
        }
        
        if(indicadora == 1){
            return true;
        }
        else
            return false;
    }
    public double dist(Ponto ponto){
        double distancia;
        distancia = Math.sqrt(Math.pow((ponto.x - this.x),2) + Math.pow((ponto.y - this.y),2) + Math.pow((ponto.z - this.z),2));
        return distancia;
    }
}
