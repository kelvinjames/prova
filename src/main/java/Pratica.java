
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ ponto1 = new PontoXZ(-3,2);
        PontoXY ponto2 = new PontoXY(0,2);
        System.out.println("Distancia = " + ponto1.dist(ponto2));
    }
    
}
